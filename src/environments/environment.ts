// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
  	apiKey: "AIzaSyBZMwP4dH4f47VnMVxMZ8R4CwrSla3Iayk",
    authDomain: "padron-cp-lima.firebaseapp.com",
    databaseURL: "https://padron-cp-lima.firebaseio.com",
    projectId: "padron-cp-lima",
    storageBucket: "padron-cp-lima.appspot.com",
    messagingSenderId: "944204219059"
  }
};