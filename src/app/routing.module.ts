import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from './services/auth-guard/auth-guard.service';


import { MainComponent } from './front/main/main.component';

import { MainComponent as AdminComponent } from './back/main/main.component';
import { LoginComponent } from './back/login/login.component';
import { NewRegisterComponent } from './back/new-register/new-register.component';
import { ListComponent } from './back/list/list.component';
import { EditComponent } from './back/edit/edit.component';




const routes: Routes = [
	{path: '', component: MainComponent},
	{path: 'login', component: LoginComponent},
	{path: 'admin', component: AdminComponent, children: [
		{path: '', redirectTo:'/admin/periodistas', pathMatch: 'full'},
		{path: 'nuevo', component: NewRegisterComponent, canActivate:[AuthGuard]},
		{path: 'periodistas', component: ListComponent, canActivate:[AuthGuard]},
		{path: 'editar/:id', component: EditComponent, canActivate:[AuthGuard]}
	]}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class RoutingModule { }
