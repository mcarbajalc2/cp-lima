import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Pressman } from '../../models/pressman';
import { PressmanService } from '../../services/pressman/pressman.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})

export class EditComponent implements OnInit {
	CPP: string;
	pressman: Pressman;

  constructor(
  	private ar: ActivatedRoute,
  	private ps: PressmanService,
  	private router: Router
	) {
  	this.ar.params.subscribe(params => {
  		this.CPP = params['id'];
  		this.getPressman();
  	});
  }

  ngOnInit() {
  }

  getPressman(){
  	this.ps.getPressmen('CPP',this.CPP).subscribe(data => {
  		for (let i in data) {
  			this.pressman = data[i];
  		}
  	});
  }

  updatePressman(){
  	let elem = <HTMLInputElement> document.getElementById("foto");
  	if (elem.files.length > 0) {
  		let foto = elem.files[0];
	  	let ref = this.ps.uploadFile(foto,this.pressman.CPP);
	  	ref.subscribe(foto_url => {
        //console.log(foto_url);
        this.ps.getPhoto(this.pressman.CPP).subscribe(foto => {
          console.log(foto);
          this.pressman.foto = foto;
          this.ps.updatePressman(this.pressman);
          this.ps.updatePressman(this.pressman).then(data => {
            this.router.navigate(["/admin"]);  
          }).catch(error => {
            console.log(error);
          });
        });	  		
	  	});
  	}else{
  		this.ps.updatePressman(this.pressman).then(data => {
  			this.router.navigate(["/admin"]);
  		});
  	}
  }

  selectFoto(){
  	let elem = <HTMLInputElement> document.getElementById("foto");
  	if (elem.files.length > 0) {
  		this.pressman.foto = elem.files[0].name;
  	}
  }

  showPhotoChooser(){
  	let elem = document.getElementById("foto");
  	elem.click();
  }
  
}
