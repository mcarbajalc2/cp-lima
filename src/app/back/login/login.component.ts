import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	public form: FormGroup;

  constructor(
  	private formBuilder: FormBuilder,
  	private authService: AuthService,
  	private router: Router) {
  	this.form = this.formBuilder.group({
  		email: ['', Validators.required],
  		password: ['',Validators.required]
  	});
  }

  ngOnInit() {
  }

  login(){
  	event.preventDefault();
  	const inputValue = this.form.value;
  	this.authService.login(inputValue.email, inputValue.password)
	  	.subscribe(
	  		success => {
	  			this.authService.prueba = 1;
	  			this.router.navigate(['/admin/periodistas']);
	  		},
	  		error => {console.log("chau")}
	  	);
  }

}
