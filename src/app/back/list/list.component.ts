import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Pressman } from '../../models/pressman';
import { PressmanService } from '../../services/pressman/pressman.service';


import * as FAS from '@fortawesome/free-solid-svg-icons';
import * as FAR from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent implements OnInit {

	fas = FAS;
	far = FAR;

	pressmen:Pressman[];
  default_foto = "https://firebasestorage.googleapis.com/v0/b/padron-cp-lima.appspot.com/o/default-user-icon-profile.png?alt=media&token=da71aea1-1a8f-45d5-be19-92e07cd50af9";

  search_form: FormGroup;

  constructor(
    private ps: PressmanService,
    private fb: FormBuilder
  ) {
    this.search_form = this.fb.group({
      search_type: ['',Validators.required],
      search_value: ['',Validators.required]
    });
    this.search_form.controls['search_type'].setValue('CPP');
  }

  ngOnInit(){
    
  }

  getPressmen(search_type,search_value){
    this.ps.getPressmen(search_type,search_value).subscribe(data => {
      this.pressmen = data;
    });
  }

  search(){
    const inputValue = this.search_form.value;
    this.getPressmen(inputValue.search_type, inputValue.search_value);
  }

  getFoto(pressman){
    if(pressman.foto){
      console.log("foto_url");
      console.log(this.ps.getFoto(pressman.CPP));
    }else{
      return this.default_foto;
    }
    return "hi";
  }

  getPhoto(pressman){
    if(pressman.foto == undefined){
      return "https://firebasestorage.googleapis.com/v0/b/padron-cp-lima.appspot.com/o/default-user-icon-profile.png?alt=media&token=da71aea1-1a8f-45d5-be19-92e07cd50af9";
    }else{
      return pressman.foto;
    }
    
  }
}
