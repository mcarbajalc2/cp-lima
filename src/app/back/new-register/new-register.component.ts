import { Component, OnInit } from '@angular/core';
import { Pressman } from '../../models/pressman';
import { PressmanService } from '../../services/pressman/pressman.service';

import { Router } from '@angular/router';


@Component({
  selector: 'app-new-register',
  templateUrl: './new-register.component.html',
  styleUrls: ['./new-register.component.css']
})
export class NewRegisterComponent implements OnInit {

	pressman:Pressman = new Pressman();
	log:Array<string> = [];

  constructor(
  	private router:Router,
  	private ps: PressmanService
	) { 
  	//console.log(this.pressman);
	}

  ngOnInit() {
  	
  }

  selectFoto(){
  	const elem = <HTMLInputElement> document.getElementById("foto");
  	if (elem.files.length > 0) {
  		this.pressman.foto = elem.files[0].name;
  	}
  }

  showPhotoChooser(){
  	const elem = document.getElementById("foto");
  	elem.click();	
  }

  addPressman(){
  	if (this.validateFields()) {
  		this.ps.getPressmen('CPP',this.pressman.CPP+"").subscribe(data => {
	  		let pressman: Pressman = undefined;
	  		for(let i in data){
	  			pressman = data[i];
	  			break;
	  		}
	  		console.log(pressman);
	  		if(pressman == undefined){  			
	  			this.ps.getPressmen('DNI',this.pressman.DNI).subscribe(data2 => {
	  				let pressman2: Pressman = undefined;
	  				for (let i in data2) {
	  					pressman2 = data2[i];
	  					break;
	  				}
	  				if(pressman2 == undefined){
	  					//guardar nuevo pressman
	  					this.saveNewPressman();
	  				}else{
	  					this.setLog("Error: ", "Se en contro un registro con el mismo DNI");
	  				}
	  			});
	  		}else{
	  			this.setLog("Error: ", "Se en contro un registro con el mismo CPP");
	  		}
	  	});	
  	}  	
  }

  saveNewPressman(){
  	if (this.pressman.foto) {
  		let elem = <HTMLInputElement> document.getElementById("foto");  		
  		this.ps.addPressman(this.pressman).then(x => {  			
  			this.ps.uploadFile(elem.files[0],this.pressman.CPP).subscribe( data => {  				
  				this.router.navigate(['admin/periodistas']);
  			});
  		});
  	}else{
  		this.ps.addPressman(this.pressman).then(x => {
  			this.router.navigate(['admin/periodistas']);
  		})
  	}
  }

  setLog(title,message){  	
  	this.log['title'] = title;
		this.log['message'] = message;
		setTimeout( fc => {
			let elem = document.getElementsByClassName('content-wrapper')[0];
			elem.scrollTop = 0;
		},200);
  }

  validateFields(){
  	const req_fields = this.pressman.getRequired();
  	let res = true;
  	for (let i in req_fields) {
			if (this.pressman[req_fields[i]] == undefined || this.pressman[req_fields[i]] == '') {
  			this.setLog('Campo '+req_fields[i]+' Requerido','Ingrese un valor en el campo '+req_fields[i]+'.');
  			res = false;
  			break;
  		}
  	}
  	return res;
  }

}
