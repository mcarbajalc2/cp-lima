export class Pressman{
	CPP: string;
	ap_paterno: string;
	ap_materno: string;
	nombre: string;
	DNI: string;
	condicion: string = "HABILITADO";
	sexo: string = "MASCULINO";
	telefono: string;
	padron: string;
	mail: string;
	date_nac: string;
	date_coleg: string;
	foto: string;

	public getRequired(): Array<any>{		
		return ['CPP','ap_paterno','ap_materno','nombre','DNI','condicion','sexo','date_nac','date_coleg'];
	}
}