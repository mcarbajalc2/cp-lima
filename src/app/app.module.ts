import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';


import { Http } from '@angular/http';
import { RoutingModule } from './routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { MainComponent } from './front/main/main.component';
import { MainComponent as AdminComponent  } from './back/main/main.component';
import { LoginComponent } from './back/login/login.component';
import { NewRegisterComponent } from './back/new-register/new-register.component';
import { ListComponent } from './back/list/list.component';

import { AuthService } from './services/auth/auth.service';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { PressmanService } from './services/pressman/pressman.service';
import { EditComponent } from './back/edit/edit.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    AdminComponent,
    LoginComponent,
    NewRegisterComponent,
    ListComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    RoutingModule,    
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [AuthService,AuthGuardService,PressmanService],
  bootstrap: [AppComponent]
})
export class AppModule { }
