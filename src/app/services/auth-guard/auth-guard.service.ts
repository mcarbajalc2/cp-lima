import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth/auth.service';


@Injectable()
export class AuthGuardService {

  constructor(
  	private authService: AuthService,
  	private router: Router) { }

  canActivate(
  	route: ActivatedRouteSnapshot,
  	state: RouterStateSnapshot  	
	){
  	if (this.authService.prueba !== 1) {
  		this.router.navigate(['/login']);
  		return false;
  	}
  	return true;
	};

}
