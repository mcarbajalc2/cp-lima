import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';

@Injectable()
export class AuthService {
	prueba:number = 0;
  constructor(private afAuth: AngularFireAuth) { }

  login(email, password){
  	return Observable.fromPromise(
  		this.afAuth.auth.signInWithEmailAndPassword(email, password)
		);
  }
}
