import { Injectable } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';

import { Observable } from 'rxjs';

import { Pressman } from '../../models/pressman';


@Injectable()
export class PressmanService {

  constructor(
    private db: AngularFireDatabase,
    private st: AngularFireStorage
  ) { }

  getPressmen(search_type: string,search_value: string):Observable<Pressman[]>{
    let sv;
  	if (search_type == '' || search_value == '') {
  		return this.db.list<Pressman>('pressman').valueChanges().pipe();
  	}else{
  		return this.db.list<Pressman>('pressman', ref => {
        sv = search_value.toUpperCase();
  			let q = ref.orderByChild(search_type).equalTo(sv);
  			return q;
  		}).valueChanges().pipe();
  	}  	
  }

  uploadFile(file: File,CPP){
    const filePath = 'fotos/'+CPP;
    const ref = this.st.ref(filePath);
    const task = ref.put(file);
    return ref.getDownloadURL().pipe();
  }

  updatePressman(pressman: Pressman){
    const ref = this.db.list('pressman');
    return ref.set("CP_"+pressman.CPP,pressman);
  }

  addPressman(pressman: Pressman){
    const ref = this.db.list('pressman');
    return ref.set("CP_"+pressman.CPP,pressman);
  }

  getFoto(CPP){
    const ref = this.st.ref('fotos/'+CPP);
    return ref.getDownloadURL().pipe();
  }

  getPhoto(CPP){
    const ref = this.st.ref('fotos/'+CPP);
    return ref.getDownloadURL().pipe();
  }

  buildDB(){
    /*
    this.db.list<Pressman>('pressman').valueChanges().subscribe(data => {
      let nuevo = new Object;
      for (let i in data) {
        const pressman:Pressman = data[i];
        pressman.CPP = ""+pressman.CPP;
        pressman.DNI = ""+pressman.DNI;
        pressman.telefono = ""+pressman.telefono;
        nuevo["CP_"+pressman.CPP] = pressman;
      }
      //console.log(nuevo);
      console.log(JSON.stringify(nuevo));
    });    
    */
  }
}
