import { TestBed, inject } from '@angular/core/testing';

import { PressmanService } from './pressman.service';

describe('PressmanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PressmanService]
    });
  });

  it('should be created', inject([PressmanService], (service: PressmanService) => {
    expect(service).toBeTruthy();
  }));
});
