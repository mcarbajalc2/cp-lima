import { Component, OnInit } from '@angular/core';
import { PressmanService } from '../../services/pressman/pressman.service';
import { Pressman } from '../../models/pressman';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
	search_type: string = "CPP";
	search_value: string;
	pressmen:Pressman[];
  constructor(private ps: PressmanService) { }

  ngOnInit() {

  }

  search(e){
  	this.ps.getPressmen(this.search_type,this.search_value)
  	.subscribe(data => {
  		this.pressmen = data;
  	});
  }
  
  cleanData(){
		this.pressmen = null;
		this.search_value = null;
	}

  getPhoto(pressman){
    if(pressman.foto == undefined){
      return "https://firebasestorage.googleapis.com/v0/b/padron-cp-lima.appspot.com/o/default-user-icon-profile.png?alt=media&token=da71aea1-1a8f-45d5-be19-92e07cd50af9";
    }else{
      return pressman.foto;
    } 
  }

}
